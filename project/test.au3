#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

#include-once
#include <include.au3>

; ---------------- add tests in this block -----------------

ConsoleWrite(_NotepadStart()[1] & @CRLF)
ConsoleWrite(_NotepadExists()[1] & @CRLF)

#include <lib\image-action\ImageSearch.au3>
$x = 0
$y = 0
_ImageSearch('Screenshot_1.jpg',0, $x, $y, 100)
ConsoleWrite($x & ' ' & $y)