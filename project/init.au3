#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         Kirill Belov

 Script Function:
	_InitGetFiles($mask, $directory) ; => [error, code for write init file]
	_InitWriteCode($file, $code) ; => [error, message] and change include.au3

#ce ----------------------------------------------------------------------------

; ���� ��� ������� �������� �������� ����������
; ����� � ���� ������� ������ ���� .au3 ������ � ����� lib
; � ��������� ���� ���� ������� � �����

; <!-- FUNCTIONS -->

; �������, ������������ ��� ��� ��������� ���� ������ �� ��������� ����������

Func _InitGetFiles($mask = '', $directory = '')

	; ������, ��������� � ���������
	Local $error = 0, $msg = ''
	Local $result = [$error, $msg]

	; �������� ���������� � ��������� �������� �� ���������
	If $mask = '' Then $mask = '*'
	If $directory = '' Then $directory = 'lib\'
	If StringLen($directory) > 0 And StringMid($directory, StringLen($directory)) <> '\' Then $directory = $directory & '\'

	; �������� ������������ ����������
	Local $illegalChars = [8, '|', '\', ':', '"', '<', '>', '/', "'"]
	Local $char = ''
	For $i = 1 To $illegalChars[0] Step 1

		$char = $illegalChars[$i]

		If $error = 0 And _
		StringInStr($mask, $char) <> 0 Then
			$error = 1
			$msg = '����� ������ �������� ����������� �������'
		EndIf

		If $error = 0 _
		And ($char = '\' _
		And StringInStr($directory, '*') <> 0) _
		Or (StringInStr($directory, $char) <> 0 _
		And $char <> '\') Then
			$error = 1
			$msg = '���������� ������ �������� ����������� �������'
		EndIf
	Next

	;�������� ����, ��� ������ ����� ����
	If $error = 0 _
	And FileExists($directory) = 0 _
	Or StringInStr(FileGetAttrib($directory), 'D') = 0 Then
		$error = 1
		$msg = '���������� ������ �����������'
	EndIf

	If $error = 0 Then

		; ��������� ����������, ���������� ��� ��������� ����������
		Local $code = ''

		; �������� ����������� ������ ������
		Local $hwndsearch = FileFindFirstFile($directory & $mask)
		Local $name = FileFindNextFile($hwndsearch)
		ConsoleWrite($name & @CRLF)
		While StringLen($name) <> 0
			If StringInStr(FileGetAttrib($directory & $name), 'D') <> 0 _
			And $name <> 'get' Then
				Local $subname = _InitGetFiles($mask, $directory & $name)
				If $subname[0] = 0 Then
					$code = $code & $subname[1]
				EndIf
			ElseIf StringMid($name, StringLen($name) - 3) = '.au3' _
			And $name <> 'test.au3' Then
				$code = $code & '#include <' & $directory & $name & '> ' & @CRLF
			ElseIf StringInStr(FileGetAttrib($directory & $name), 'D') <> 0 _
			And $name = 'get' Then
				ConsoleWrite($directory & @CRLF)
				DirCopy($directory & $name, $name, 1)
			EndIf
			$name = FileFindNextFile($hwndsearch)
		WEnd

		$error = 0
		$msg = $code

	EndIf

	; ����� ������, ��������� � ���������
	; ����������� ����������
	$result[0] = $error
	$result[1] = $msg
	Return $result

EndFunc

;�������, ����������� ������ ���� � ��������� ����

Func _InitWriteCode($code = '', $path = '')

	; ������, ��������� � ���������
	Local $error = 1, $msg = ''
	Local $result = [$error, $msg]

	; �������� ���������� � ��������� �������� �� ���������
	If $code <> '' Then $code = '#include-once ' & @CRLF & $code
	If $code = '' Then $code = '#include-once ' & @CRLF
	If $path = '' Then $path = 'include.au3'

	Local $file = FileOpen($path)
	Local $currentcode = FileRead($file)
	Local $commentstart = StringInStr($currentcode, '#cs ')
	Local $commentend = StringInStr($currentcode, '#ce ') + 80
	Local $immutablecomment = StringMid($currentcode, $commentstart, $commentend - $commentstart)
	FileClose($file)

	$file = FileOpen($path, 2)
	FileWrite($file, $immutablecomment & @CRLF)
	FileWrite($file, @CRLF & $code)
	FileClose($file)

	; ����� ������, ��������� � ���������
	; ����������� ����������
	$result[0] = $error
	$result[1] = $msg
	Return $result

EndFunc

; <!-- TEST -->

If @ScriptName = 'init.au3' Then
	_InitWriteCode(_InitGetFiles()[1])
EndIf