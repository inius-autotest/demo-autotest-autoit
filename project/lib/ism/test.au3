#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

#include-once
#include <ImageSearch.au3>

$res = _ImageSearchInWindow('get\image.png')
If $res[0] = 0 Then
	$coordinate = $res[1]
	MouseMove($coordinate[0], $coordinate[1])
Else
	ConsoleWrite($res[1])
EndIf