#include-once

Func _ImageSearchInWindow($findImage, $winhandle = '', $tolerance = '')

	Local $result[2] = [0, '']

	Local $winpos = 0

	If $tolerance = '' Then $tolerance = 10
	If $winhandle = '' Then
		$winhandle = WinGetHandle('active')
		Local $desktoppos = [0, 0, @DesktopWidth, @DesktopHeight]
		$winpos = $desktoppos
	EndIf

	If WinExists($winhandle) Then
		WinActivate($winhandle)
		If $winpos = 0 Then $winpos = WinGetPos($winhandle)
		$left = $winpos[0]
		$top = $winpos[1]
		$right = $winpos[0] + $winpos[2]
		$bottom = $winpos[1] + $winpos[3]
		If $tolerance > 0 Then $findImage = "*" & $tolerance & " " & $findImage
		Local $imageposition = DllCall("get\ImageSearchDLL.dll","str","ImageSearch","int",$left,"int",$top,"int",$right,"int",$bottom,"str",$findImage)
		If $imageposition[0] = '0' Then
			$error = 1
			$msg = '����������� �� ���� �������'
		Else
			Local $array = StringSplit($imageposition[0], '|')
			Local $x = Int(Number($array[2])) + Int(Number($array[4])/2)
			Local $y = Int(Number($array[3])) + Int(Number($array[5])/2)
			Local $coordinate[2] = [$x, $y]
			$error = 0
			$msg = $coordinate
		EndIf
	Else
		$error = 1
		$msg = '����������� ���� � ������� ���������� ��������� �����'
	EndIf

	$result[0] = $error
	$result[1] = $msg
	Return $result

EndFunc