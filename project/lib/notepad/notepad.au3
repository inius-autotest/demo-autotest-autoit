#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

Func _NotepadExists()

	; ���������� ������, ��������� � ����������
	Local $error = 0, $msg = ''
	Local $result[2] = [$error, $msg]

	; ���������� ������� ��������� � ���� ��������
	Local $notepadPidList = ProcessList('notepad.exe')
	Local $notepadWins = WinList('[CLASS:Notepad]')

	; ������� ������� ��� �������� ������������� �������� � ������ ������

	; ������� ���������� ���� � ��������
	If $notepadPidList[0][0] = 0 _
	And $notepadWins[0][0] = 0 Then
		$error = 1
		$msg = '������� �� �������'

	; ������� ������������� �������� ��� ����
	ElseIf $notepadPidList[0][0] > 0 _
	And $notepadWins[0][0] = 0 Then
		$error = 1
		$msg = '���������� ������� ��������, ����������� ���� ��������'

	; ���������� ���� ��� ��������
	ElseIf $notepadPidList[0][0] = 0 _
	And $notepadWins[0][0] > 0 Then
		$error = 1
		$msg = '����������� ������� ��������, ����������� ���� ��������'

	; ��������� ���������� ���� � ��������� ��������
	ElseIf $notepadPidList[0][0] <> $notepadWins[0][0] Then
		$error = 1
		$msg = '���������� ���������� ��������� � ���� �� ���������'

	; ������������ ������ ��� ���� �������
	ElseIf $notepadPidList[0][0] > 1 _
	And $notepadWins[0][0] > 1 Then
		$error = 1
		$msg = '���������� ��������� ���������� ����������� ��������'

	; ���������� ���������� ������� �������� �� ����������� � ����
	ElseIf $notepadPidList[0][0] = 1 _
	And $notepadWins[0][0] = 1 _
	And WinGetProcess($notepadWins[1][1]) <> $notepadPidList[1][1] Then
		$error = 1
		$msg = '���������� ������� �������� �� ��������� � ��������� ����'

	; ������������ ������� �� ���������� ������
	; ������� �������� � ���� ���� ���� � ��� ����
	; ���� ��������� ������ � ����� ��������
	ElseIf $notepadPidList[0][0] = 1 _
	And $notepadWins[0][0] = 1 _
	And WinGetProcess($notepadWins[1][1]) = $notepadPidList[1][1] Then
		$error = 0
		$msg = '������� �������'

	;���� ������ �� ������� - ����������� ������
	Else
		$error = 1
		$msg = '����������� ������'
	EndIf

	; ����� ������ � ��������� � ���������
	$result[0] = $error
	$result[1] = $msg
	Return $result

EndFunc

Func _NotepadStart()

	; ��������� ����������� ������, ��������� � ����������
	Local $error = 0, $msg = ''
	Local $result[2] = [$error, $msg]

	Local $notepadExists = _NotepadExists()

	If $notepadExists[0] = 1 _
	And $notepadExists[1] = '������� �� �������' Then
		$notepadPid = Run('notepad.exe')
		ProcessWait($notepadPid)
		$notepadExists = _NotepadExists()
		If $notepadExists[0] = 0 Then
			$error = $notepadExists[0]
			$msg = '������� ����������'
		Else
			$error = $notepadExists[0]
			$msg = $notepadExists[1]
		EndIf
	Else
		$error = $notepadExists[0]
		$msg = $notepadExists[1]
	EndIf
	; ����� ������ � ��������� � ���������
	$result[0] = $error
	$result[1] = $msg
	Return $result

EndFunc

If @ScriptName = 'notepad.au3' Then
	ConsoleWrite(_NotepadExists()[1] & @CRLF)
	ConsoleWrite(_NotepadStart()[1] & @CRLF)
	ConsoleWrite(_NotepadExists()[1] & @CRLF)
EndIf