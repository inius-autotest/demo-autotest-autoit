#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         Kirill Belov

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

; <!-- CONSTANTS -->

Global $default[2] = [0, '']
Global $funcname = ''
Global $logtest = 'console'

; <!-- FUNCTIONS -->

; �������, ������� ��������� �������� ��������� �� ������������

Func _ArgCheck($arg)

	; ������, ��������� � ���������
	Local $error = 0, $msg = ''
	Local $result[2] = [$error, $msg]

	; �������� ������� ������������
	If IsArray($arg) <> 0 _
	And UBound($arg) = 2 _
	And IsNumber($arg[0]) <> 0 _
	And ($arg[0] = 0 _
	Or $arg[0] = 1) Then
		$error = 0
		$msg = '���������� ��������'
	Else
		$error = 1
		$msg = '������ ���������'
	EndIf

	; ����� ������, ��������� � ���������
	$result[0] = $error
	$result[1] = $msg
	Return $result

EndFunc

; �������-������ - � ����������� �� �������� ����������� �������� ���������� ������
; ����������� � ��������� �������� �������
; � ������ �������, ����� ������ ��������� ������, ����
Func _LogWrite($error, $msg, $func = '', $log = '')
	If $func = '' Then $func = $funcname
	If $log = '' Then $log = $logtest
	If $log = 'console' Then ConsoleWrite($func & ' : ' & $error & '|' & $msg & ' (' & @YEAR & '.' & @MON & '.' & @MDAY & ' ' & @HOUR & ':' & @MIN & ':' & @SEC & ')' & @CRLF)
EndFunc

; �������-������� - ��������� �������� ������ �������������� �������
; ��� ����� ���������� �� � �������� �������, ������������� �������
Func _AuxWrap()

	Local $error = 0, $msg = ''
	Local $result[0] = [$error, $msg]

	Local $argcheck = _ArgCheck($arg)

	If $argcheck[0] = 0 Then
		$error = $argcheck[0]
		$msg = $msg
	Else
		$error = $argcheck[0]
		$msg = $argcheck[1]
	EndIf

	If $error = 0 Then

	EndIf

	Local $func = 'fun'
	$result[0] = $error
	$result[1] = $msg
	Return $result

EndFunc

_ArgCheck($default)